.PHONY: build install clean

VERSION = 0.1

DESTDIR = /
PREFIX = /usr

INSTALL_FLAGS = --destdir=$(DESTDIR) \
				--prefix=$(PREFIX)

BUILDER = python -m build
INSTALLER = python -m installer

MANDIR = $(PREFIX)/share/man

WHL_FILE = dist/sqlite_to_cpp-$(VERSION)-py3-none-any.whl

build: $(WHL_FILE)

$(WHL_FILE): setup.cfg
	$(BUILDER) --no-isolation --wheel

install: build sqlite-to-cpp.1
	$(INSTALLER) $(INSTALL_FLAGS) $(WHL_FILE)
	install -D -m 644 -t $(DESTDIR)/$(MANDIR)/man1 sqlite-to-cpp.1

setup.cfg: setup.cfg.in
	sed \
		-e 's/@@version@@/$(VERSION)/g' \
		$< > $@

sqlite-to-cpp.1: sqlite-to-cpp.1.in
	sed \
		-e 's/@@version@@/v$(VERSION)/g' \
		-e "s/@@date@@/$$(date -I)/g" \
		$< > $@

